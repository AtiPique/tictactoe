#include <stdio.h>

#include "printgrid.h"

void printgrid(char grid[][3]) {

    printf("\n");
    printf(" %c | %c | %c \n", grid[0][0], grid[0][1], grid[0][2]);
    printf("---|---|---\n");
    printf(" %c | %c | %c \n", grid[1][0], grid[1][1], grid[1][2]);
    printf("---|---|---\n");
    printf(" %c | %c | %c \n", grid[2][0], grid[2][1], grid[2][2]);

}
