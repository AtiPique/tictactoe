#include <stdio.h>

#include "printgrid.h"
#include "move.h"
#include "victorydetection.h"

char grid[3][3] = {{' ', ' ', ' '}, 
                   {' ', ' ', ' '}, 
                   {' ', ' ', ' '}};

void clearScreen();
int isGridComplete(char grid[][3]); 


int main(int argc, char* argv[]) {
    int count = 0;

    game:

        printf("%d", count);

        clearScreen();
        
        printgrid(grid);

        move(grid, 'X');
        
        count++;

        printgrid(grid);

        move(grid, 'O');

        count++;

        char winner = ' ';

        if (victoryDetection(grid, 'X') == 'X') {
            winner = 'X';
            printf("%c", winner);
        } else if (victoryDetection(grid, 'O') == 'O') {
            winner = 'O';
            printf("%c", winner);
        }

        if (winner == ' ') {
            goto game;
        } else {
            printgrid(grid);
            printf("Winned by %c\n");
        }

        if (count == 9) {
            printf("There's no winner !\n");
        }


    return 0;
}

void clearScreen() {
    system("clear");
}